﻿using Nature.Models;
using Nature.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using Urho.Resources;
using Urho.Urho2D;

namespace Nature
{

    enum KeyPressedMap
    {
        Key_None = 0,
        Key_W = 1,
        Key_A = 2, 
        Key_S = 3, 
        Key_D = 4
    }

    public class CameraControl
    {
        private Node CameraNode;
        private UI UI;
        private Input Input;

        protected Vector3 baseSp;  //base camera speed
        protected Vector3 currentMoveMultiplier; 
        protected Vector3 initMoveMultiplier;
        protected float panMovementDeaccelXY; //camera xy pan deacceleration speed
        protected Vector3 maxMoveMultiplier;
        protected Vector3 panMovementAccel;

        //vector for calculating current camera pan/zoom offset
        private Vector3 movementDelta = new Vector3(0, 0, 0);

        //used for disabling deacceleration during camera pan/zoom
        private bool xMovement = false;
        private bool yMovement = false;
        private bool zMovement = false;


        //check if input given, used for acceleration building
        private bool KeyPressed = false;

        List<KeyPressedMap> keysPressedTable = new List<KeyPressedMap>();

        KeyPressedMap keyPressed = 0;

        public CameraControl(Node cameranode, UI ui, Input input, Vector3 baseSpeed, Vector3 accelPan, float deaccelPanXY, Vector3 maxmult)
        {
            this.CameraNode = cameranode;
            this.UI = ui;
            this.Input = input;
            this.initMoveMultiplier = new Vector3(1f, 1f, 1f);
            this.baseSp = baseSpeed;
            this.panMovementAccel = accelPan;
            this.panMovementDeaccelXY = deaccelPanXY;
            this.maxMoveMultiplier = maxmult;
            this.currentMoveMultiplier = new Vector3(1f, 1f, 1f);
        }


        /// <summary>
        /// 2D camera control
        /// </summary>
        /// <param name="timeStep"></param>
        /// <param name="accel"></param>
        public void ProcessCameraMovement2D(float timeStep)
        {
            // Do not move if the UI has a focused element (the console)
            if (UI.FocusElement != null) return;

            // Movement speed as world units per second
            const float moveSpeed = 14.0f;

            // Read WASD keys and move the camera scene node to the corresponding direction if they are pressed
            if (Input.GetKeyDown(Key.W))
            {
                yMovement = true; //disable Y movement axis deacceleration
                movementDelta.Y += panMovementAccel.Y * moveSpeed * timeStep;
                keyPressed = KeyPressedMap.Key_W;
            }

            if (Input.GetKeyDown(Key.S))
            {
                yMovement = true; //disable Y movement axis deacceleration
                movementDelta.Y -= panMovementAccel.Y * moveSpeed * timeStep;
                keyPressed = KeyPressedMap.Key_S;
            }

            if (Input.GetKeyDown(Key.A))
            {
                xMovement = true; //disable X movement axis deacceleration
                movementDelta.X -= panMovementAccel.X * moveSpeed * timeStep;
                keyPressed = KeyPressedMap.Key_A;
            }

            if (Input.GetKeyDown(Key.D))
            {
                xMovement = true; //disable X movement axis deacceleration
                movementDelta.X += panMovementAccel.X * moveSpeed * timeStep;
                keyPressed = KeyPressedMap.Key_D;
            };


            //apply deacceleration
            if (!xMovement && movementDelta.X !=0)
            {
                movementDelta.X -= movementDelta.X / Math.Abs(movementDelta.X) * panMovementDeaccelXY * timeStep;
                if (Math.Abs(movementDelta.X) < 0.1)
                {
                    movementDelta.X = 0;
                }
            }

            if (!yMovement && movementDelta.Y !=0)
            {
                movementDelta.Y -= movementDelta.Y / Math.Abs(movementDelta.Y) * panMovementDeaccelXY * timeStep;

                if (Math.Abs(movementDelta.Y) < 0.1)
                {
                    movementDelta.Y = 0;
                }
            }

            //clear keys pressed table
            keysPressedTable.Clear();
          

            //clear camera movement flags (enabling deacceleration)
            xMovement = false;
            yMovement = false;

        CameraNode.Translate(movementDelta);
        }

        public Camera CreateCamera()
        {
            return CameraNode.CreateComponent<Camera>();
        }

        public string ReturnDebugText()
        {
            string s = String.Format($"{this.currentMoveMultiplier.X} {this.currentMoveMultiplier.Y} {this.currentMoveMultiplier.Z}");
            return s;
        }

        public string ReturnPosText()
        {
            string x = string.Format("{0:F1}", CameraNode.Position.X);
            string y = string.Format("{0:F1}", CameraNode.Position.Y);
            
            string s = string.Format($"{x} {y}");
            return s;
        }

    }
}
