﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nature.Debug
{

    public enum ViewModes
    {
        ModeDefault = 0,
        ModeSafety = 1, 
        ModeHunger = 2, 
        ModeFatigue = 3
    }


    public static class DebugStats
    {
        public static int EntityCount { get; set; }
        public static int ViewMode { get; set; }

        private static string DebugText { get; set; }
    }
}
