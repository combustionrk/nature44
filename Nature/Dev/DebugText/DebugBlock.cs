﻿using Nature.Dev.DebugText;
using Nature.Models;
using Nature.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using Urho.Resources;
using Urho.Urho2D;


namespace Nature.Dev.DebugText
{
    public class DebugBlock
    {
        public UI Ui { get; set; }
        private Text _text = new Text();
        public Font Font { get; set; }
        public IntVector2 Position2D { get; set; }

        //hide/show flag
        public bool Show { get; set; }

        //list with stored debug line entries
        private List<DebugEntry> _debugEntries = new List<DebugEntry>();

        public DebugBlock()
        {

        }

        public DebugBlock(UI ui)
        {
            this.Ui = ui;
        }
        
        public void Init()
        {
            // _text.Value = this.ToString();
            _text.Value = "";
            _text.HorizontalAlignment = HorizontalAlignment.Left;
            _text.VerticalAlignment = VerticalAlignment.Top;
            _text.Position = this.Position2D;
            _text.Name = "SlapText";
            _text.SetColor(new Color(1.0f, 1.0f, 0.0f));
            _text.SetFont(Font, 24);
            Ui.Root.AddChild(_text);
        }

        public void Update()
        {
            _text.Value = this.ToString();
        }

        /// <summary>
        /// Adding debug line entry
        /// </summary>
        /// <param name="de"></param>
        public void AddEntry(DebugEntry de)
        {
            _debugEntries.Add(de);
        }
        
        /// <summary>
        /// Remove debug line entry by its name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>success status</returns>
        public bool RemoveEntry(string name)
        {
            var selected = _debugEntries.SingleOrDefault(x => x.Name == name);

            if (selected != null)
            {
                _debugEntries.Remove(selected);
                return true;
            }
            return false;
        }

        public void SetValue(string name, string value)
        {
            var selected = _debugEntries.SingleOrDefault(x => x.Name == name);
            if (selected != null)
            {
                selected.Text = value;
            }
        }

        public override string ToString()
        {
            string output = "";
            //var selected = _debugEntries.Select(x => output += x.Text + "\n");

            foreach (var x in _debugEntries)
            {
                output += x.Text + "\n";
            }

            return output;
        }
    }
}
