﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nature.Dev.DebugText
{
    public class DebugEntry
    {
        public string Name { get; set; }
        public string Text { get; set; }

        public DebugEntry()
        {

        }

        public DebugEntry(string name, string text)
        {
            this.Name = name;
            this.Text = text;
        }
    }
}
