﻿using Nature.Models;
using Nature.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using Urho.Resources;
using Urho.Urho2D;

namespace Nature.Dev
{
        /// <summary>
        /// debug element (key or value)
        /// </summary>
        public class DebugElement
        {
            public string Text { get; set; }
            public Color Color { get; set; }
            public int FontSize { get; set; }

            public DebugElement()
            {
                this.Text = "";
                this.Color = new Color(1f, 1f, 1f);
                this.FontSize = 16;
            }
        }

        /// <summary>
        /// key value pair
        /// </summary>
        public class DebugEntry 
        {
            public DebugElement Key { get; set; }
            public DebugElement Value { get; set; }
        }

         
        public class DebugLine
        {
            public string Name { get; set; }
            public List<DebugEntry> entries = new List<DebugEntry>();
        }

        /// <summary>
        /// block of debug text lines
        /// </summary>
        public class DebugTextBlock
        {
           public UI Ui { get; set; }
           public IntVector2 Position { get; set; }
           public Font DebugFont { get; set; }
           public bool Show { get; set; }
           public List<DebugLine> debugLines = new List<DebugLine>();

           private Text _debugText;

            public DebugTextBlock()
            {
                this.Position = new IntVector2(10, 10);
                this.Show = true;
            }

            public void Init()
            {
                _debugText = new Text();
                _debugText.Value = "placeholder"; //fill it with formatted lines from list
                _debugText.HorizontalAlignment = HorizontalAlignment.Left;
                _debugText.VerticalAlignment = VerticalAlignment.Top;
                _debugText.Position = Position;
                _debugText.Name = "DebugBlock";
                _debugText.SetColor(new Color(0xff, 0x88, 0));
                // _debugText.SetFont(ResourceCache.GetFont("Fonts/Anonymous Pro.ttf"), 14);
                _debugText.SetFont(DebugFont, 14);

                Ui.Root.AddChild(_debugText);
            }

            public void AddEntry(DebugLine dl)
            {
                int count = debugLines.Count;
                dl.Name = String.Format($"DebugLine{count}");
                debugLines.Add(dl);
            }

            /// <summary>
            /// removes debug line by its name
            /// </summary>
            /// <param name="name"></param>
            /// <returns></returns>
            public bool RemoveEntry(string name)
            {
                DebugLine selected = debugLines.SingleOrDefault(x => x.Name == name);
                if (selected != null)
                {
                    debugLines.Remove(selected);
                    return true;
                }
                return false;
            }
    }
}
