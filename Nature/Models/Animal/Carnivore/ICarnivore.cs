﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nature.Models.Animal.Carnivore
{
    public interface ICarnivore
    {
         void Hunt();
    }
}
