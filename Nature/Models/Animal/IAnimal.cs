﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;

namespace Nature.Models
{
    public interface IAnimal
    {
        void WalkTo(Vector3 Pos);
        void Sleep();
    }
}
