﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using Urho.Resources;
using Urho.Urho2D;

namespace Nature.Models
{
    public class NodeInfo
    {
        public Node Node { get; set; }
        public Vector3 MoveSpeed { get; set; }
        public float RotateSpeed { get; set; }

        public NodeInfo(Node node, Vector3 moveSpeed, float rotateSpeed)
        {
            Node = node;
            MoveSpeed = moveSpeed;
            RotateSpeed = rotateSpeed;
        }
    }
}
