﻿using Nature;
using Nature.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenTest
{
    class Program
    {
        static void Main(string[] args)
        {
            WorldGen wg = new WorldGen(15, 10);
            wg.GenerateCross();
            var cutVersion = UtilMatrix.ReturnSmallMatrix(wg.Matrix, new int[] { 0, 0 }, new int[] { 5, 5 });
            UtilMatrix.Display(wg.Matrix);
            UtilMatrix.Display(cutVersion);


            int testNumber = Int32.MaxValue;
            testNumber++;
            Console.WriteLine(testNumber);

            Console.ReadKey();
        }
    }
}
