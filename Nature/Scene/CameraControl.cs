﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nature.Scene
{
    public class CameraControl
    {
        public CameraControl()
        {

        }

        void SimpleMoveCamera2D(float timeStep, Vector3 accel)
        {
            // Do not move if the UI has a focused element (the console)
            if (UI.FocusElement != null)
                return;

            // Movement speed as world units per second
            const float moveSpeed = 4.0f;

            // Read WASD keys and move the camera scene node to the corresponding direction if they are pressed
            if (Input.GetKeyDown(Key.W)) CameraNode.Translate(Vector3.UnitY * moveSpeed * timeStep * accel.Y);
            if (Input.GetKeyDown(Key.S)) CameraNode.Translate(-Vector3.UnitY * moveSpeed * timeStep * accel.Y);
            if (Input.GetKeyDown(Key.A)) CameraNode.Translate(-Vector3.UnitX * moveSpeed * timeStep * accel.X);
            if (Input.GetKeyDown(Key.D)) CameraNode.Translate(Vector3.UnitX * moveSpeed * timeStep * accel.X);

            if (Input.GetKeyDown(Key.PageUp))
            {
                CameraControl camera = CameraNode.GetComponent<CameraControl>();
                camera.Zoom = camera.Zoom * 1.01f * accel.Z;
            }

            if (Input.GetKeyDown(Key.PageDown))
            {
                CameraControl camera = CameraNode.GetComponent<CameraControl>();
                camera.Zoom = camera.Zoom * 0.99f * accel.Z;
            }
        }
    }
}
