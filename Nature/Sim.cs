﻿using Nature.Dev.DebugText;
using Nature.Models;
using Nature.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using Urho.Resources;
using Urho.Urho2D;

namespace Nature
{
    public class Sim : Application //: Sample
    {
        UrhoConsole console;
        DebugHud debugHud;
        ResourceCache cache;
        Sprite logoSprite;
        UI ui;
        protected MonoDebugHud MonoDebugHud;
        DebugBlock db;

        Scene scene;
        Camera camera;
        CameraControl camControl;

        Text debugText;

        //public parameters
        public List<NodeInfo> AnimalList { get; set; } 
        public World SimWorld { get; set; }

    
        protected Node CameraNode { get; set; }

        Sprite gesintuvasSprite;

        public Sim(ApplicationOptions options = null) : base(options)
        {
            AnimalList = new List<NodeInfo>((int)1000);
            SimWorld = new World(5000,5000);
        }

        void CreateConsoleAndDebugHud()
        {
            var xml = cache.GetXmlFile("UI/DefaultStyle.xml");
            console = Engine.CreateConsole();
            console.DefaultStyle = xml;
            console.Background.Opacity = 0.8f;

            debugHud = Engine.CreateDebugHud();
            debugHud.DefaultStyle = xml;
        }

        void HandleKeyDown(KeyDownEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Esc:
                    Exit();
                    return;
                case Key.F1:
                    console.Toggle();
                    return;
                case Key.F2:
                    debugHud.ToggleAll();
                    return;
            }

            var renderer = Renderer;
            switch (e.Key)
            {
                case Key.N1:
                    var quality = renderer.TextureQuality;
                    ++quality;
                    if (quality > 2)
                        quality = 0;
                    renderer.TextureQuality = quality;
                    break;

                case Key.N2:
                    var mquality = renderer.MaterialQuality;
                    ++mquality;
                    if (mquality > 2)
                        mquality = 0;
                    renderer.MaterialQuality = mquality;
                    break;

                case Key.N3:
                    renderer.SpecularLighting = !renderer.SpecularLighting;
                    break;

                case Key.N4:
                    renderer.DrawShadows = !renderer.DrawShadows;
                    break;

                case Key.N5:
                    var shadowMapSize = renderer.ShadowMapSize;
                    shadowMapSize *= 2;
                    if (shadowMapSize > 2048)
                        shadowMapSize = 512;
                    renderer.ShadowMapSize = shadowMapSize;
                    break;

                // shadow depth and filtering quality
                case Key.N6:
                    var q = (int)renderer.ShadowQuality++;
                    if (q > 3)
                        q = 0;
                    renderer.ShadowQuality = (ShadowQuality)q;
                    break;

                // occlusion culling
                case Key.N7:
                    var o = !(renderer.MaxOccluderTriangles > 0);
                    renderer.MaxOccluderTriangles = o ? 5000 : 0;
                    break;

                // instancing
                case Key.N8:
                    renderer.DynamicInstancing = !renderer.DynamicInstancing;
                    break;

                case Key.N9:
                    Image screenshot = new Image();
                    Graphics.TakeScreenShot(screenshot);
                    screenshot.SavePNG(FileSystem.ProgramDir + $"Data/Screenshot_{GetType().Name}_{DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss", CultureInfo.InvariantCulture)}.png");
                    break;
            }
        }


        protected override void Start()
        {
            cache = ResourceCache;

            /*
            Log.LogMessage += e => Debug.WriteLine($"[{e.Level}] {e.Message}");
            base.Start();
            if (Platform == Platforms.Android ||
                Platform == Platforms.iOS ||
                Options.TouchEmulation)
            {
                InitTouchInput();
            }
            Input.Enabled = true;
            MonoDebugHud = new MonoDebugHud(this);
            MonoDebugHud.Show();

            CreateLogo();
            SetWindowAndTitleIcon();
            CreateConsoleAndDebugHud();
            Input.SubscribeToKeyDown(HandleKeyDown);

            */


            Input.Enabled = true;
            CreateConsoleAndDebugHud();
            Input.SubscribeToKeyDown(HandleKeyDown);



            MonoDebugHud = new MonoDebugHud(this);
            MonoDebugHud.Show();

            CreateScene();
            //CreateUI();
            SetupViewport();

            // mousewheel zoom event
            SubscribeToEvents();

            //init camera control
            camControl = new CameraControl(CameraNode, UI, Input, new Vector3 (14f, 14f, 14f), new Vector3(0.15f, 0.15f, 0.15f), 4f,  new Vector3(10, 10, 10));
           
            db = new DebugBlock(UI);
            db.Position2D = new IntVector2(10, 50);
            db.Font = ResourceCache.GetFont("Fonts/Anonymous Pro.ttf");
            db.AddEntry(new DebugEntry("CamPos", "Cam Pos = "));
            db.AddEntry(new DebugEntry("Second", "Cam Speed = "));
            db.Init();

            // CreateSprites();
        }

        private void CreateScene()
        {
            scene = new Scene();
            scene.CreateComponent<Octree>();

            //camera setup
            CameraNode = scene.CreateChild("Camera");
            CameraNode.Position = (new Vector3(0.0f, 0.0f, -10.0f));
            camera = CameraNode.CreateComponent<Camera>();
            camera.Orthographic = false;

            var graphics = Graphics;
            camera.OrthoSize = (float)graphics.Height * PixelSize;

            //halfwidth 
            float halfWidth = graphics.Width * 0.5f * PixelSize;
            float halfHeight = graphics.Height * 0.5f * PixelSize;


            #region terrain
            //terrain plane
            
            var planeNode = scene.CreateChild("Plane");

            planeNode.Scale = new Vector3(SimWorld.Dimensions.X, 1, SimWorld.Dimensions.Y);
           

            //planeNode.Scale = new Vector3(300, 1, 300);
            planeNode.Rotate(new Quaternion(90, 0, 180));
           

            var planeObject = planeNode.CreateComponent<StaticModel>();
            planeObject.Model = cache.GetModel("Models/Plane.mdl");

            //set material and tiling
            Material terrainMaterial = ResourceCache.GetMaterial("R/Materials/GrassTiled.xml");
            terrainMaterial.SetUVTransform(new Vector2(0, 0), 0f, new Vector2(SimWorld.Dimensions.X / 16,SimWorld.Dimensions.Y/16));
            planeObject.SetMaterial(terrainMaterial);

            #endregion

            #region light
            var lightNode = scene.CreateChild("DirectionalLight");
            lightNode.SetDirection(new Vector3(0.6f, -1.0f, 0.8f)); // The direction vector does not need to be normalized
            var light = lightNode.CreateComponent<Light>();
            light.LightType = LightType.Directional;
            #endregion

            Sprite2D sprite = ResourceCache.GetSprite2D("wolf.png");

            #region initWolves
            for (uint i = 0; i < 1000; ++i)
            {
                Node spriteNode = scene.CreateChild("StaticSprite2D");
                spriteNode.Position = (new Vector3(UtilNumeric.NextRandom(-100, 100), UtilNumeric.NextRandom(-100, 100), -0.1f));

                StaticSprite2D staticSprite = spriteNode.CreateComponent<StaticSprite2D>();
                // Set random color
                staticSprite.Color = (new Color(UtilNumeric.NextRandom(1.0f), UtilNumeric.NextRandom(1.0f), UtilNumeric.NextRandom(1.0f), 1.0f));
                // Set blend mode
                staticSprite.BlendMode = BlendMode.Alpha;
                // Set sprite
                staticSprite.Sprite = sprite;
                // Add to sprite node vector
                //  animalList.Add(new NodeInfo(spriteNode, new Vector3(UtilNumeric.NextRandom(-2.0f, 2.0f), UtilNumeric.NextRandom(-2.0f, 2.0f), 0.0f), UtilNumeric.NextRandom(-90.0f, 90.0f)));

               AnimalList.Add(new NodeInfo(spriteNode, new Vector3(UtilNumeric.NextRandom(-2.0f, 2.0f), UtilNumeric.NextRandom(-2.0f, 2.0f), 0.0f), UtilNumeric.NextRandom(0f, 0f)));
            }
            #endregion

        }

        //main render loop
        protected override void OnUpdate(float timeStep)
        {
            camControl.ProcessCameraMovement2D(timeStep);

            var graphics = Graphics;
            float halfWidth = graphics.Width * 0.5f * PixelSize;
            float halfHeight = graphics.Height * 0.5f * PixelSize;
            




            //render sprites
            foreach (var nodeInfo in AnimalList)
            {
                Vector3 position = nodeInfo.Node.Position;
                Vector3 moveSpeed = nodeInfo.MoveSpeed;
                Vector3 newPosition = position + moveSpeed * timeStep;
                if (newPosition.X < -100 || newPosition.X > 100)
                {
                    newPosition.X = position.X;
                    moveSpeed.X = -moveSpeed.X;
                    nodeInfo.MoveSpeed = moveSpeed;
                }
                if (newPosition.Y < -100 || newPosition.Y > 100)
                {
                    newPosition.Y = position.Y;
                    moveSpeed.Y = -moveSpeed.Y;
                    nodeInfo.MoveSpeed = moveSpeed;
                }

                nodeInfo.Node.Position = (newPosition);
                nodeInfo.Node.Roll(nodeInfo.RotateSpeed * timeStep, TransformSpace.Local);
            }


            //debugText.Value = camControl.ReturnDebugText();
            db.SetValue("CamPos", string.Format($"Camera Pos = {camControl.ReturnPosText()}"));
            db.Update();
            
            base.OnUpdate(timeStep);
        }

        private void CreateUI()
        {
            throw new NotImplementedException();
        }

        private void SubscribeToEvents()
        {
        //cc.SubscribeToMouseWheelEvents();
            Input.SubscribeToMouseWheel(args => CameraNode.Translate(-Vector3.UnitZ * 1f * args.Wheel * -1));
            
        }

        void SetupViewport()
        {
            var renderer = Renderer;
            renderer.SetViewport(0, new Viewport(Context, scene, CameraNode.GetComponent<Camera>(), null));
        }

        private void CreateSprites()
        {
            //terrain tex
            Sprite2D terrainSp = new Sprite2D();
            Texture2D terrainTexture = ResourceCache.GetTexture2D("terrain_desert.jpg");
            terrainSp.Texture = terrainTexture;

            //coon
            Texture2D coonTexture = ResourceCache.GetTexture2D("coon.jpg");
            Sprite coonSprite = new Sprite();
            coonSprite.Texture = coonTexture;

            coonSprite.Position = new IntVector2(100, 100);
            coonSprite.HotSpot = new IntVector2(50, 50);
            coonSprite.Size = new IntVector2(200, 200);
            UI.Root.AddChild(coonSprite);

            //gesintuvas
            Texture2D gesintuvasTexture = ResourceCache.GetTexture2D("gesintuvas_small.jpg");
            gesintuvasSprite = new Sprite();
            gesintuvasSprite.Texture = gesintuvasTexture;
            gesintuvasSprite.Position = new IntVector2(600, 100);
            gesintuvasSprite.HotSpot = new IntVector2(400, 400);
            gesintuvasSprite.Size = new IntVector2(800, 800);
            UI.Root.AddChild(gesintuvasSprite);

            //wolf
            Texture2D deerTexture = ResourceCache.GetTexture2D("wolf.png");
            Sprite deerSprite = new Sprite();
            deerSprite.Texture = deerTexture;
            deerSprite.Position = new IntVector2(600, 600);
            deerSprite.HotSpot = new IntVector2(300, 300);
            deerSprite.Size = new IntVector2(200, 200);
            deerSprite.BlendMode = BlendMode.Alpha;
            UI.Root.AddChild(deerSprite);

            //terrain node
            Node groundNode = scene.CreateChild("Terrain");
            groundNode.Position = (new Vector3(0.0f, -3.0f, 0.0f));
            groundNode.Scale = new Vector3(200.0f, 1.0f, 0.0f);
            groundNode.CreateComponent<RigidBody2D>();
        }
    }
}
