﻿using Nature.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;

namespace Nature.Utils
{
    public static class UtilMatrix
    {
        /// <summary>
        /// checks if matrix dimensions are valid values
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static bool ValidateDimensions(int width, int height)
        {
            if (width > 0 && height > 0)
            {
                return true;
            }
            return false;
        }

        public static bool ValidateMatrixCoord(int[,] matrix, int x, int y)
        {
            var dim = GetMatrixDimensions(matrix);

            if (x > 0 && x < dim[0] && y > 0 && y < dim[1])
            {
                return true;
            }
            return false;
        }

        public static int[,] GenEmptyMatrix(int width, int height, int value = 0)
        {
            int[,] resultVector = new int[width, height];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    resultVector[i, j] = value;
                }
            }
            return resultVector;
        }

        public static int[] GetCenterTileCoord(int[,] matrix)
        {
            int[] dimm = GetMatrixDimensions(matrix);
            int[] centerDataLocation = new int[2];
            centerDataLocation[0] = (int)Math.Floor((double)dimm[0] / 2);
            centerDataLocation[1] = (int)Math.Floor((double)dimm[1] / 2);
            return centerDataLocation;
        }


        /// <summary>
        /// cuts and returns smaller matrix, out of bigger one
        /// </summary>
        /// <param name="matrix">source matrix</param>
        /// <param name="topLeft">topleft pos</param>
        /// <param name="bottomRight">bottomright pos</param>
        /// <returns></returns>
        public static int[,] ReturnSmallMatrix(int[,] matrix, int[] topLeft, int[] bottomRight)
        {
            int newWidth = bottomRight[0] - topLeft[0] + 1;
            int newHight = bottomRight[1] - topLeft[1] + 1;

            int[] dim = GetMatrixDimensions(matrix);

            if (newWidth < 1 || newHight < 1 || newWidth > dim[0] || newHight > dim[1])
            {
                return null;
            }

            int[,] result = new int[newWidth, newHight];

            for (int i = 0; i < newHight; i++)
            {
                for (int j = 0; j < newWidth; j++)
                {
                    int val = GetMatrixElementValue(matrix, topLeft[0] + j, topLeft[1] + i);
                    SetMatrixElementValue(result, j, i, val);
                }
            }
            return result;
        }

        /// <summary>
        /// returns element and its surrounding neighbour cells in 3x3 matrix
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static int[,] ReturnElementAndNeighboursMatrix(int[,] matrix, int x, int y)
        {
            int[,] matrixSmall = ReturnSmallMatrix(matrix, new int[2] { x - 1, y - 1 }, new int[2] { x + 1, y + 1 });
            return matrixSmall;
        }


        /// <summary>
        /// fills rectangular area of specified matrix with specified value
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="topLeft"></param>
        /// <param name="bottomRight"></param>
        /// <param name="value"></param>
        public static void FilLCustomMatrix(int[,] matrix, int[] topLeft, int[] bottomRight, int value)
        {
            var dims = GetMatrixDimensions(matrix);

            int minX = Math.Max(0, topLeft[0]);
            int maxX = Math.Min(dims[0], bottomRight[0]);

            int minY = Math.Max(0, topLeft[1]);
            int maxY = Math.Min(dims[1], bottomRight[1]);

            for (int i = minX; i < (maxX + 1); i++)
            {
                for (int j = minY; j < (maxY + 1); j++)
                {
                    SetMatrixElementValue(matrix, i, j, value);
                }
            }
        }

        /// <summary>
        /// returns matrix dimmensions (int[2] array)
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static int[] GetMatrixDimensions(int[,] matrix)
        {
            int width = matrix.GetLength(0);
            int height = matrix.GetLength(1);
            int[] result = new int[2];
            result[0] = width;
            result[1] = height;
            return result;
        }

        /// <summary>
        /// gets element value in matrix, at specified location
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static int GetMatrixElementValue(int[,] matrix, int x, int y)
        {
            if (!ValidateMatrixCoord(matrix, x, y))
            {
                return -1;
            }
            else
            {
                return matrix[x, y];
            }
        }

        /// <summary>
        /// Sets matrix value at specified location
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="value"></param>
        public static void SetMatrixElementValue(int[,] matrix, int x, int y, int value)
        {
            if (ValidateMatrixCoord(matrix, x, y))
            {
                matrix[x, y] = value;
            }
        }

        /// <summary>
        /// Display matrix using console output
        /// </summary>
        /// <param name="matrix"></param>
        public static void Display(int[,] matrix)
        {
            int[] dims = GetMatrixDimensions(matrix);

            for (int i = 0; i < dims[1]; i++)
            {
                for (int j = 0; j < dims[0]; j++)
                {
                    Console.Write(matrix[j, i]);
                    Console.Write(" ");
                }
                Console.WriteLine();
            }

            Console.WriteLine($"Width = {dims[0]}, Height = {dims[1]}");

            var center = GetCenterTileCoord(matrix);
            Console.WriteLine($"Center tile : {center[0]}  {center[1]}");
        }
    }
}
