﻿using Nature.Models;
using System.Collections.Generic;
using Urho;

public class Fauna
{
    public List<IAnimal> Objects { get; set; }

    public Fauna()
    {
        Objects = new List<IAnimal>();
    }

}