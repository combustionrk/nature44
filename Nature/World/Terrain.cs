﻿using Urho;
using Urho.Resources;

namespace Nature
{
    public class Terrain
    {
        private Vector2 _dimmensions;

        public Material Mat { get; set; }
        public Vector2 Dimmensions
        {
            get { return _dimmensions; }
            set
            {
                if (value.X > 0 && value.Y > 0)
                {
                    _dimmensions = value;
                }
            }
        }

        public Terrain()
        {
            Dimmensions = new Vector2(100, 100);
        }

        public Terrain(int x, int y)
        {
            Dimmensions = new Vector2(x, y);
            Mat = new Material();
        }

    }
}
