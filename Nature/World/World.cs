﻿using Urho;

public class World
{
    //world bounds
    private Vector2 _dimensions;
    public Vector2 Dimensions
    {
        get { return this._dimensions; }
        set
        {
            if (value.X > 0 && value.Y > 0)
            {
                _dimensions = value;
            }
        }
    }

    //3 major elements
    public Terrain Terrain { get; set; }
    public Fauna Fauna { get; set; }
    public Flora Flora { get; set; }

    public World()
    {
        this.Dimensions = new Vector2(100, 100);
    }

    public World(int x, int y)
    {
        this.Dimensions = new Vector2(x, y);
    }

}
