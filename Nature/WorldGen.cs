﻿using Nature.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urho;

namespace Nature
{
    public class WorldGen
    {
        public int Width { get; private set; }
        public int Height { get; private set; }

        public int[,] Matrix { get; private set; }

        public WorldGen(int width, int height)
        {
            if (UtilMatrix.ValidateDimensions(width, height))
            {
                this.Width = width;
                this.Height = height;
                Matrix = UtilMatrix.GenEmptyMatrix(width, height, 0);

                int[] dims = UtilMatrix.GetMatrixDimensions(Matrix);
                Width = dims[0];
                Height = dims[1];
            }
        }

        public int GetNeighbourCount(int x, int y)
        {
            var smallMatrix = UtilMatrix.ReturnElementAndNeighboursMatrix(Matrix, x, y);
            int ncount = 0;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (Matrix[i,j] > 0) {
                        ncount++;
                    }
                }
            }
            return ncount;
        }

        public void GenerateCross()
        {
            var dim = UtilMatrix.GetMatrixDimensions(Matrix);

            //generate max tile growth in 4 directions
            int MaxTilesNorth = UtilNumeric.NextRandom(0, dim[1] / 2);
            int MaxTilesEast = UtilNumeric.NextRandom(0, dim[0] / 2);
            int MaxTilesSouth = UtilNumeric.NextRandom(0, dim[1] / 2);
            int MaxTilesWest = UtilNumeric.NextRandom(0, dim[0] / 2);

            //center point
            int[] centerCoord = UtilMatrix.GetCenterTileCoord(Matrix);
            UtilMatrix.SetMatrixElementValue(Matrix, centerCoord[0], centerCoord[1], 1);

            //north
            for (int i = 0; i < MaxTilesNorth; i++)
            {
                UtilMatrix.SetMatrixElementValue(Matrix, centerCoord[0], centerCoord[1] - i, 1);
            }
            //east
            for (int i = 0; i < MaxTilesEast; i++)
            {
                UtilMatrix.SetMatrixElementValue(Matrix, centerCoord[0] + i, centerCoord[1], 1);
            }
            //south
            for (int i = 0; i < MaxTilesSouth; i++)
            {
                UtilMatrix.SetMatrixElementValue(Matrix, centerCoord[0], centerCoord[1] + i, 1);
            }
            //west
            for (int i = 0; i < MaxTilesWest; i++)
            {
                UtilMatrix.SetMatrixElementValue(Matrix, centerCoord[0] - i, centerCoord[1], 1);
            }
        }
    }
}
