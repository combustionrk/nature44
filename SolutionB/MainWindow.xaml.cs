﻿using Nature;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Urho;
using Urho.Desktop;

namespace SolutionB
{
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            DesktopUrhoInitializer.AssetsDirectory = @"../../Assets";
            Loaded += SolutionBInit;
        }

        private void SolutionBInit(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(RunGame);
        }

        async void RunGame()
        {
            var app = await UrhoSurfaceCtrl.Show(typeof(Sim));
            Urho.Application.InvokeOnMain(() => { });
        }
    }
}
